* xref:index.adoc[Contributing]
** For writers:
*** xref:prerequisites.adoc[Prerequisites]
*** xref:7step-gitlab-howto.adoc[7 Step HowTo for casual contributors]
*** xref:docs-workflow.adoc[The Docs Workflow organization]
//** xref:repo-structure.adoc[Repository Structure]
*** xref:contributing-to-existing-docs.adoc[Contributing to Existing Documentation]
*** xref:contributing-to-release-notes.adoc[Contributing to Release Notes]
*** xref:adding-new-docs.adoc[Adding New Documentation to the Site]
*** xref:local-preview.adoc[Building a Local Preview]
*** xref:git.adoc[Git for docs writers]
//** xref:working-with-translations.adoc[Working with Translations]
** For designers:
*** xref:design-ux.adoc[Design & UX]
