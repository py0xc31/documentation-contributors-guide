= Fedora Documentation Style Guide

[NOTE]
====
Editors note: First draft to be discussed. Please do not quote publicly and do not link publicly.
====


== Typografic style guide 

Use capitalization case for title (h1) and just title:: Capatilize the article title, but just the title.
+
* _Incorrect_: Fedora documentation style guide
* _Correct_: Fedora Documentation Style Guide
    
Use sentence case for the post title and heading titles:: Don’t capitalize words in your article title or any heading, other than proper nouns.
+
* _Incorrect_: Technical Notes and Processes
* _Correct_: Technical notes and processes 

Less is more:: Use boldface only for an extremely important phrase or statement. Use italics to emphazise text. 

Use italics for system objects you mention in a sentence::
+
  GUI or CLI elements like button text or menu entries
  other prompts the reader must find on the screen commands or package names

Use the preformatted source text for command line input or output:: Use a shell prompt ($ or #) to indicate priviledge level and set the input apart from output. It also helps to use boldface for the input itself.

  [source,]
  ----
  […]# command arg1 arg2
  output line 1
  output line2
  ----

Use Admonitions very sparingly:: They strongly interrupt the reading flow and thus make it difficult to grasp a section of text or even the article as a whole in a meaningful way. 

== Writing style guide

Write clearly and use shorter sentences:: Brevity is good. Clarity is better. Don’t be excessively wordy when avoidable. If a longer sentence is easier to read, use the extra words.

Avoid passive voice:: Passive voice is the use of the object of a sentence as the subject. For example
* _Active voice_: The troops defeated the enemy.
* _Passive voice_: The enemy was defeated by the troops.

Be careful of gerunds (-ing words):: They usually indicate passive voice. Rewrite your sentence to make it stronger. For example:
* _Weak, passive voice_: Setting the foobar configuration option will make the application listen on all interfaces.
* _Strong, active voice_: Set the foobar configuration option to make the application listen on all interfaces.

Avoid unnecessary future tense:: Unless you’re actually talking about future plans or publications, present tense is best.
* _Unnecessary future tense_: When you select Run, your program will start.
* _Present tense_: When you select Run, your program starts.

Avoid too much use of the verb to be in sentences:: Too much use of is, will be, or can be makes your sentences weak and flabby. Try using the verb form of words you’ve shuffled off elsewhere in the sentence. Often you can simply drop words, or use the imperative (commanding or advising) form of the sentence.
* _Weak_: Zambone is an app used for managing your private documents on a server.
* _Strong_: Zambone manages your private documents on a server. Or: Use Zambone to manage your private documents on a server.
* _Weak_: When setting up a file server, it is important to plan the directory structure carefully.
* _Strong_: Plan the directory structure of the file server carefully before you set it up.

Use standard US English for spelling and other international differences:: US English is the lingua franca for the Fedora Project overall.

Have a smooth flow from general information to specific instructions:: If you’re not sure how to structure your article like this, check out our starter template.

Avoid long, bulky texts:: Such texts are daunting and take time to make sense of. 
+
Instead, organize the text using paragraphs, indentations, or lists. This helps the user to quickly grasp the text and, above all, does not seem daunting.  

Provide a short abstract at the beginning of a longer collection of paragraphs:: Especially after second and third order headings ("h2" and "h3", "==" and "===" in Asciidoc), a short sentence or paragraph should follow, briefly describing the goal and/or subject of the following paragraphs, i.e. clearly bringing the "message" to the reader's attention and aligning the reader's expectation accordingly.  

== Content tips 

These tips are about things to do — and avoid — in what you tell users to do. Remember that thousands of readers trust Fedora Documentation to tell them how to carry out tasks. Be responsible and helpful, test your examples carefully, advocate best practices, and respect the user’s security and choice.

Test your process:: If possible, use a fresh guest VM — or at least a brand-new user account. Run your process from beginning to end to ensure it works. Fix, rinse, and repeat!

Prefer free software where practical (and officially packaged software wherever possible):: We can cover non-FOSS software to be used on Fedora, where we know or suspect that software is very popular and useful to Fedora users. (Google Chrome or Nvidia driver are good examples.) But we strongly prefer to use FOSS software.

Unless necessary, use Fedora family distributions:: We are Fedora. Unless your documentation article specifically targets a cross-distribution mechanism, use installations, containers, or distributions within our family (Fedora, CentOS, RHEL).

Copr software must be accompanied by a caveat:: The Copr build system is not managed by the Fedora release team and does not provide official software builds. If you cover some software from Copr, ensure there’s no official build. If not, include a statement like this: _Copr is not officially supported by Fedora infrastructure. Use packages at your own risk_. 

Avoid exclusionary or problematic language:: These are examples of terminology to avoid wherever possible in articles:
+
* blacklist/whitelist — Use allowlist/denylist instead, which is more directly descriptive of the purpose.
* master/slave — Use primary/secondary, primary/replica, active/passive, active/standby, or another similar construct.

Use the correct style for third parties:: Names of companies, projects, and technologies don’t always follow the style rules of typical English words. Choose the styling used by authoritiative websites when you’re unsure. If authoritaive sources use inconsistent style, use your best judgment. A non-exhaustive list:

* _Copr_ instead of COPR
* _NVIDIA_ instead of Nvidia or nVidia
* _Perl_ instead of PERL
* _Red Hat_ instead of Redhat or RedHat
* _ThinkPad_ instead of Thinkpad


== Images and screenshots

Use a fresh, standard Fedora system:: Don't use your personal system or setup. It’s best to make a VM with a fresh Fedora variant install, and do the steps there. 

Set screen resolution at a reasonable but not too high resolution:: Stay beyond resolutions as 1280x960 or 1280x800.

If you’re only showing a browser window, make it fairly large on the screen and screenshot only the browser:: You can use the Screenshot app for this!

If you’re only showing an application, use the default size of the app to screenshot it::

Upload and use that original media in your article:: If the shot is large, like a large browser window, app, or whole screen, choose a medium size thumbnail and let Antorra CMS handle the conversion.



== Processing tips

Check spelling and grammar:: Check your work before creating a Pull Request.